package br.ucsal.testequalidade.restaurante.business;

public class RestauranteBOTest {

	/**
	 * Método a ser testado:
	 * 
	 * public Integer fecharComanda(Integer codigoComanda) throws
	 * RegistroNaoEncontrado, ComandaFechadaException {
	 * 
	 * Verificar se o fechamento de uma comanda que não está aberta levanta a
	 * exceção ComandaFechadaException.
	 * 
	 * Deve ser veficada, além da ocorrência da exceção, que a mensagem contida na
	 * mesma é "Comanda já fechada (código = <código-comanda>).", onde
	 * <código-comanda> é o código da comanda passada para o método fecharComanda.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Comanda;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao;
	 * 
	 * 3. O teste deve ser UNITÁRIO.
	 * 
	 */
	public void testarFecharComandaJaFechada() {
	}

}
