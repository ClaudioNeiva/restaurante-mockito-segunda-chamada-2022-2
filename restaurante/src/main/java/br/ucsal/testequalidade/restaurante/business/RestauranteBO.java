package br.ucsal.testequalidade.restaurante.business;

import br.ucsal.testequalidade.restaurante.domain.Comanda;
import br.ucsal.testequalidade.restaurante.domain.Item;
import br.ucsal.testequalidade.restaurante.domain.Mesa;
import br.ucsal.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBO {

	private MesaDao mesaDao;

	private ComandaDao comandaDao;

	private ItemDao itemDao;

	public RestauranteBO(MesaDao mesaDao, ComandaDao comandaDao, ItemDao itemDao) {
		this.mesaDao = mesaDao;
		this.comandaDao = comandaDao;
		this.itemDao = itemDao;
	}

	public Integer abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException {
		Mesa mesa = mesaDao.obterPorNumero(numeroMesa);
		if (SituacaoMesaEnum.LIVRE.equals(mesa.getSituacao())) {
			Comanda comanda = new Comanda();
			comanda.setMesa(mesa);
			comandaDao.incluir(comanda);
			mesa.setSituacao(SituacaoMesaEnum.OCUPADA);
			return comanda.getCodigo();
		}
		throw new MesaOcupadaException(numeroMesa);
	}

	/**
	 * Este método adiciona a uma comanda um item à uma comanda, caso ele ainda não esteja nela. 
	 * Caso o item já exista, este método adicionará a quantidade ao item já existente.
	 * 
	 * @param codigoComanda - código da comanda que terá o item adicionado;
	 * @param codigoItem    - código do item que será adicionado (ou terá a quantidade incrementada);
	 * @param qtdItem       - quantidade do item que será adicionado;
	 * @throws RegistroNaoEncontrado
	 */
	public void incluirItemComanda(Integer codigoComanda, Integer codigoItem, Integer qtdItem) throws RegistroNaoEncontrado {
		Integer qtdAtual = 0;
		Comanda comanda = comandaDao.obterPorCodigo(codigoComanda);
		Item item = itemDao.obterPorCodigo(codigoItem);
		if (comanda.getItens().containsKey(item)) {
			qtdAtual = comanda.getItens().get(item);
		}
		qtdAtual += qtdItem;
		comanda.getItens().put(item, qtdAtual);
	}

	public Double calcularTotal(Comanda comanda) {
		Double total = 0d;
		for (Item item : comanda.getItens().keySet()) {
			total += item.getValorUnitario() * comanda.getItens().get(item);
		}
		return total;
	}

	public Double fecharComanda(Integer codigoComanda) throws RegistroNaoEncontrado, ComandaFechadaException {
		Comanda comanda = comandaDao.obterPorCodigo(codigoComanda);
		if (SituacaoComandaEnum.ABERTA.equals(comanda.getSituacao())) {
			comanda.setSituacao(SituacaoComandaEnum.FECHADA);
			return calcularTotal(comanda);
		}
		throw new ComandaFechadaException(codigoComanda);
	}

}
